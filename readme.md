Run 'make all' to build and install everything. If you're using linux you will have problems with static linking and are probably missing libbsd. After installing libbsd you should edit the various .c files by commenting out the pledge() syscalls or it will not build. Run 'make linux' to build dynamically linked binaries. There are no 'install' commands for the linux builds as these are primarily for debugging purposes. 

Debugging make targets have been added as an example. 

Do not run this in production, there are no checks to prevent attempting to load an infinitely large post request into memory. 

Linux is supported only for nice debugging features like source-level debugging and valgrind. I have not tested it extensively on linux. 
