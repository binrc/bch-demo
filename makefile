default: 
	@echo "Run 'make all' to build and install everything. If you're using linux you will have problems with static linking and are probably missing libbsd. After installing libbsd you should edit the various .c files by commenting out the pledge() syscalls or it will not build. Run 'make linux' to build dynamically linked binaries. There are no 'install' commands for the linux builds as these are primarily for debugging purposes. " | fold -s -w 80

index: 
	cc -g -static index.c -o index.cgi
	install -o www -g www -m 0500 index.cgi /var/www/cgi-bin/demo

colors-get: 
	cc -g -static colors-get.c -o colors-get.cgi
	install -o www -g www -m 0500 colors-get.cgi /var/www/cgi-bin/demo

colors-post: 
	cc -g -static colors-post.c -o colors-post.cgi
	install -o www -g www -m 0500 colors-post.cgi /var/www/cgi-bin/demo

all: index colors-get colors-post

debug-index: 
	gdb index.cgi

memcheck-index: 
	valgrind --leak-check=full --track-origins=yes ./index.cgi

debug-get: 
	QUERY_STRING="fg=00ff00&bg=000000" gdb colors-get.cgi

memcheck-get: 
	QUERY_STRING="fg=00ff00&bg=000000" valgrind --leak-check=full --track-origins=yes ./colors-get.cgi

debug-post: 
	CONTENT_LENGTH="20" gdb -ex=r --args ./colors-post.cgi < postrequest

memcheck-post: 
	CONTENT_LENGTH="20" valgrind --leak-check=full --track-origins=yes ./colors-post.cgi < postrequest

linux: 
	@printf "\033[5;41;97mlol this guy doesn't have static linking or lbsd\033[0m\n"
	cc -ggdb -DLIBBSD_OVERLAY -I/usr/include/bsd  ./index.c  -o index.cgi -lbsd
	cc -ggdb -DLIBBSD_OVERLAY -I/usr/include/bsd  ./colors-get.c  -o colors-get.cgi -lbsd
	cc -ggdb -DLIBBSD_OVERLAY -I/usr/include/bsd  ./colors-post.c  -o colors-post.cgi -lbsd

