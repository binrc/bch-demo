#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void){
	// nice stuff you don't get on linux
	if(pledge("stdio", NULL) == -1)
		exit(1);

	// required
	puts("Status: 200 OK\r");
	puts("Content-Type: text/html\r");
	puts("\r");


	printf("<p> Colors (GET) example: </p> \
		<form action='colors-get.cgi' method='get'> \
		<label for='fg'>Foreground</label> \
		<input name='fg' maxlength='6' required><br> \
		<label for='bg'>Background</label> \
		<input name='bg' maxlength='6' required><br> \
		<input type='submit' value='submit'> \
		</form> \
		<hr>");

	printf("<p> Colors (POST) example: </p> \
		<form action='colors-post.cgi' method='post'> \
		<label for='fg'>Foreground</label> \
		<input name='fg' maxlength='6' required><br> \
		<label for='bg'>Background</label> \
		<input name='bg' maxlength='6' required><br> \
		<input type='submit' value='submit'> \
		</form> \
		<hr>");


	return 0;

}
