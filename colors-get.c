#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv, char **envp){
	// nice stuff you don't get on linux
	if(pledge("stdio", NULL) == -1)
		exit(1);
	
	// required
	puts("Status: 200 OK\r");
	puts("Content-Type: text/html\r");
	puts("\r");


	// get request string
	char *req = getenv("QUERY_STRING");
	if(req == NULL) return 1;

	int reqlen = strlen(req);

	// allocate memory
	/* more memory is allocated than we actually need
	 * but this will prevent overflowing if someone decides
	 * to send us a bunch of unexpected  garbage. 
	 * Additionally we will create a copy of the query string
	 * because string operations can be destructive. */
	char *fg;
	char *bg;
	char *reqcp = malloc(sizeof(char) * (reqlen + 1));
	strlcpy(reqcp, req, sizeof(char) * (reqlen + 1));
	char *ptr2reqcp = reqcp; 	// need to maintain a pointer to our string because strsep mutilates pointers
	

	// slice request string
	/* first we tokenize the string then we copy everything
	 * after the '=' character. To get rid of the '=' character 
	 * we can either iterate the pointer or change it to a '#' 
	 * because it will be used as a hex color code. */

	char *tok;
	int field = 0;

	while((tok = strsep(&reqcp, "&")) && field < 2){
		switch(field){
			case 0: 
				fg = strchr(tok, '=');
				break;
			case 1: 
				bg = strchr(tok, '=');
				break;
		}
		field++;
	}

	fg[0] = '#'; 	// alternatively: fg++; to remove the first character of the string
	bg[0] = '#';


	// now we actually print the page
	printf("<body style='color: %s; background-color: %s;'>\n", fg, bg);
	
	// print the environmental variables
	for(char **env = envp; *env != 0; env++){
		printf("<p>%s<p>\n", *env);
	}

	printf("</body>");

	// clean up
	free(ptr2reqcp);

	return 0;

}
